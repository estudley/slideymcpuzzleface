import QtQuick 2.11
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.4
import QtQuick.Window 2.11

Window {
    id: root
    visible: true
    width: game.width
    height: game.height

    signal move(var direction)

    Action { shortcut: "Up"; onTriggered: move("Up") }
    Action { shortcut: "Down"; onTriggered: move("Down") }
    Action { shortcut: "Left"; onTriggered: move("Left") }
    Action { shortcut: "Right"; onTriggered: move("Right") }

    FileDialog {
        visible: true
        title: "Select an image for the puzzle"
        folder: shortcuts.pictures

        onAccepted: appManager.puzzleFilename = fileUrl

        onRejected: {
            console.log("Canceled")
            Qt.quit()
        }
    }

    Rectangle {
        id: game
        width: 245
        height: width
        anchors { centerIn: parent }
        color: "#263740"

        Repeater {
            model: appManager.pieceCount

            Image {
                id: piece
                x: initialX
                y: initialY
                sourceSize: Qt.size(game.width / appManager.columnCount, game.height / appManager.rowCount)

                property int initialX: (index % appManager.columnCount) * width
                property int initialY: Math.floor((index / appManager.rowCount)) * height

                property bool solved: x === initialX && y === initialY

                property point newPos

                function move(x, y) {
                    newPos = Qt.point(x, y)
                    anim.start()
                }

                function updateSource() {
                    source = "image://imageprovider/" + index
                }

                SequentialAnimation {
                    id: anim
                    SmoothedAnimation { target: piece; property: "x"; to: newPos.x; duration: 2000 }
                    SmoothedAnimation { target: piece; property: "y"; to: newPos.y; duration: 2000 }

                    onRunningChanged: {
                        if (index === (appManager.pieceCount - 1)) {
                            var solved = true
                            for (var i = 0; i < appManager.pieceCount; i++) {
                                var objX = (i % appManager.columnCount) * width
                                var objY = Math.floor((i / appManager.rowCount)) * height

                                var obj = game.childAt(objX, objY)
                                if (obj && !obj.solved) {
                                    solved = false
                                }
                            }

                            console.log(solved)
                        }
                    }
                }

                Connections {
                    target: root

                    onMove: {
                        if (index === (appManager.pieceCount - 1) && !anim.running) {
                            var obj
                            switch (direction) {
                            case "Up":    obj = game.childAt(x, y + height); break
                            case "Down":  obj = game.childAt(x, y - height); break
                            case "Left":  obj = game.childAt(x + width, y); break
                            case "Right": obj = game.childAt(x - width, y); break
                            }

                            if (obj) {
                                var tempX = obj.x
                                var tempY = obj.y
                                obj.move(x, y)
                                move(tempX, tempY)
                            }
                        }
                    }
                }

                Connections {
                    target: appManager

                    onPuzzleFilenameChanged: piece.updateSource()
                }
            }
        }
    }
}
