#include "AppManager.h"

#include <QDir>
#include <QDebug>

AppManager::AppManager(ImageType type, Flags flags) : QQuickImageProvider(type, flags)
{
//    m_puzzle = QImage("C:/Users/estudley/Documents/Code/Qt/SlideyMcPuzzleface/images/puzzle.png");
    setPieceCount(25);
}

QImage AppManager::requestImage(const QString& id, QSize* size, const QSize& requestedSize)
{
    Q_UNUSED(size)

    QImage piece(requestedSize, QImage::Format_ARGB32);

    piece.fill(QColor("transparent"));

    if (!m_puzzle.isNull()) {
        int index = id.toInt();
        int lastIndex = (m_rowCount * m_columnCount) - 1;

        if (index != lastIndex) {
            int row = index % m_rowCount;
            int column = index / m_columnCount;

            int x = row * requestedSize.width();
            int y = column * requestedSize.height();

            piece = m_puzzle.copy(x, y, requestedSize.width(), requestedSize.height());
        }
    }

    return piece;
}

void AppManager::setPieceCount(int pieceCount)
{
    m_pieceCount = pieceCount;
    m_rowCount = qRound(qSqrt(pieceCount));
    m_columnCount = qRound(qSqrt(pieceCount));

    emit pieceCountChanged(m_pieceCount);
    emit rowCountChanged(m_rowCount);
    emit columnCountChanged(m_columnCount);
}

void AppManager::setPuzzleFilename(QString puzzleFilename)
{
    if (m_puzzleFilename == puzzleFilename)
        return;

    m_puzzleFilename = puzzleFilename;

    m_puzzle = QImage(QDir::toNativeSeparators(QUrl(puzzleFilename).toLocalFile()));

    emit puzzleFilenameChanged(m_puzzleFilename);
}
