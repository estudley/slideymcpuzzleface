#include <QQmlContext>
#include <QGuiApplication>
#include <QQuickImageProvider>
#include <QQmlApplicationEngine>

#include "AppManager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    app.setOrganizationName("organizationName");
    app.setOrganizationDomain("organizationDomain");

    AppManager *m_appCtrl = new AppManager(QQuickImageProvider::Image);

    engine.addImageProvider(QLatin1String("imageprovider"), m_appCtrl);
    engine.rootContext()->setContextProperty("appManager", m_appCtrl);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
